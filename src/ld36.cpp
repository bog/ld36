#include <iostream>
#include <SFML/Graphics.hpp>
#include <Core.hpp>

int main()
{
  sf::VideoMode mode = sf::VideoMode::getDesktopMode();

  mode.width = 3*mode.width/4;
  mode.height = 3*mode.height/4;
    
  sf::RenderWindow window(mode, "LD34", sf::Style::Titlebar
			  | sf::Style::Close);

  window.setVerticalSyncEnabled(true);
  
  Core core (&window);
  core.run();
  
  return EXIT_SUCCESS;
}
