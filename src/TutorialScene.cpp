/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


#include <TutorialScene.hpp>

TutorialScene::TutorialScene(Core* core)
  : Scene(core)
  , m_input_delay (500)
{
  sf::Vector2f win = { (float) m_core->window()->getView().getSize().x
		       , (float) m_core->window()->getView().getSize().y};
  m_bg.setSize(win);
  m_bg.setPosition({0, 0});
  m_bg.setTexture( m_core->rsc()->getTexture("tutorial_bg") );
  m_music = m_core->rsc()->getMusic("tutorial");
  m_music->play();
}

void TutorialScene::update(float dt)
{
  UNUSED(dt);
  
  if( m_input_clock.getElapsedTime().asMilliseconds() >= m_input_delay )
    {
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) )
	{
	  m_input_clock.restart();
	  m_music->stop();
	  m_core->sceneMenu();
	}
    }
}

void TutorialScene::display()
{
  m_core->window()->draw(m_bg);
}

TutorialScene::~TutorialScene()
{
  
}
