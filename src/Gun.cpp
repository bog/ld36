/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Gun.hpp>
#include <Actor.hpp>
#include <ArenaScene.hpp>

Gun::Gun(Scene* scene
	 , Actor* actor
	 , unsigned int shot_delay
	 , sf::Color rayline_color
	 , std::string texture_name
	 , std::string sound_name
	 , std::string sound_reflect_name)
  : Item(scene, actor)
  , m_shot_delay (shot_delay)
  , m_rayline_color ( rayline_color )
{
  m_size << m_core->rsc()->getTexture(texture_name)->getSize().x,
    m_core->rsc()->getTexture(texture_name)->getSize().y;
  m_size *= 2;
  m_dir << 1, 0;
  
  m_shot_dist = std::max(m_core->window()->getSize().x
			 , m_core->window()->getSize().y);
  
  m_shape.setTexture( m_core->rsc()->getTexture(texture_name) );
  m_shot_sound.setBuffer( *m_core->rsc()->getSoundBuffer(sound_name) );
  m_reflect_sound.setBuffer( *m_core->rsc()->getSoundBuffer(sound_reflect_name) );
}

/*virtual*/ void Gun::update(float dt)
{
  UNUSED(dt);
    
  Item::update(dt);
  
  m_dir = m_actor->getLookDir();
  
  {
    Eigen::Vector3f dir3 (m_dir(0), m_dir(1), 0);
    Eigen::Vector3f ortho3 = dir3.cross( Eigen::Vector3f(0, 0, -1) );
    Eigen::Vector2f ortho ( ortho3(0), ortho3(1) );
    ortho.normalize();
    
    m_shot_point = m_pos
      + m_dir * m_size(0)
      + ortho * m_size(1)/2;
    
    float radius = std::min( m_size(0), m_size(1) )/2;
    m_shot_point_shape.setRadius(radius/10);
    m_shot_point_shape.setPosition({m_shot_point(0) - radius/10
	  , m_shot_point(1) - radius/10});
    m_shot_point_shape.setFillColor(sf::Color::Yellow);
  }
}

/*virtual*/ void Gun::display()
{
  m_core->window()->draw(m_shape);
}

/*virtual*/ void Gun::shot()
{
  if(m_shot_clock.getElapsedTime().asMilliseconds() < m_shot_delay)
    {
      return;
    }
  
  ArenaScene* scene = static_cast<ArenaScene*>(m_scene);
  Eigen::Vector2f target = m_shot_point + m_dir * m_shot_dist;
  
  m_shot_sound.play();
  m_shot_clock.restart();

  scene->buddyShot(m_actor, m_shot_point, target);
}

/*virtual*/ void Gun::reflect()
{
  m_reflect_sound.play();
}

/*virtual*/ bool Gun::busy() const
{
  return (m_reflect_sound.getStatus() == sf::SoundSource::Playing
	  || m_shot_sound.getStatus() == sf::SoundSource::Playing);
}

Gun::~Gun()
{
  
}
