#include <Item.hpp>
#include <Actor.hpp>

Item::Item(Scene* scene, Actor* actor)
  : Entity (scene)
  , m_actor (actor)
  , m_angle (0)
{
  m_offset << 0, 0;

}

/*virtual*/ void Item::update(float dt)
{
  UNUSED(dt);
  
  Eigen::Vector2f apos = m_actor->getPos();
  Eigen::Vector3f dir3;
  dir3 << m_actor->getLookDir()(0), m_actor->getLookDir()(1), 0;
  dir3 *= -1 * ( m_actor->getRadius() - m_size(1)/2 );
  
  Eigen::Vector3f item_offset3 = dir3.cross( Eigen::Vector3f(0, 0, 1) );
  
  m_offset << item_offset3(0), item_offset3(1);
  m_pos = apos + m_offset;

  m_shape.setSize({m_size(0), m_size(1)});  
  m_shape.setPosition({m_pos(0), m_pos(1)});

  Eigen::Vector2f u(1, 0);
  m_angle = acos( m_dir.dot(u) );

  if(m_dir(1) < 0){ m_angle = 2*M_PI - m_angle; }

  m_shape.setRotation( DEG(m_angle) );
}

void Item::setAngle(float angle)
{
  m_angle = angle;
}

/*virtual*/ void Item::display()
{
}


Item::~Item()
{
  
}
