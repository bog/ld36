/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <ArenaScene.hpp>

ArenaScene::ArenaScene(Core* core)
  : Scene(core)
  , m_baddies_wave (0)
  , m_max_baddies_wave (30)
{
  m_player = std::make_unique<Player>(this);
  
  float yratio = 0.9;
  
  m_main_view = sf::View({0
	, 0
	, static_cast<float>(m_core->window()->getSize().x)
	, yratio * static_cast<float>(m_core->window()->getSize().y)});
  
  m_main_view.setViewport({0, 1-yratio, 1, yratio});

  m_background.setSize( m_main_view.getSize() );
  m_background.setTexture( m_core->rsc()->getTexture("arena_bg") );
  
  sf::FloatRect life_rect (0
			   , 0
			   , (float) m_core->window()->getSize().x
			   , (float) m_core->window()->getSize().y * (1-yratio)
			   );
  
  m_life_bar = std::make_unique<LifeBar>(this, m_player.get(), life_rect);
  
  m_ray_cache = std::make_unique<RayCache>(this);
  m_effect_cache = std::make_unique<EffectCache>(this);
  
  m_music = m_core->rsc()->getMusic("arena");
  m_music->setLoop(true);
  m_music->play();
  
  newWave();
}

void ArenaScene::addRandomBaddies(size_t n)
{
  for(size_t i=0; i<n; i++)
    {
      Baddy* baddy = nullptr;
      Eigen::Vector2f dir;

      do
	{
	  dir << m_core->random(-1, 2)
	    , m_core->random(-1, 2);
	}
      while ( dir == Eigen::Vector2f::Zero() );
 
      dir.normalize();
  
      Eigen::Vector2f middle;
      middle << m_core->window()->getSize().x/2
	, m_core->window()->getSize().y/2;
  
      float max = std::max( m_core->window()->getSize().x/2
			    , m_core->window()->getSize().y/2 );
      Eigen::Vector2f pos;
      pos = middle + dir * m_core->random(max, 2*(max) + 1);

      unsigned int type = m_core->random(0, 100);

      if(type <= 20)
	{
	  baddy = new SniperBaddy( this, pos );
	}
      else if(type > 20 && type <= 50)
	{
	  baddy = new LightBaddy( this, pos );
	}
      else
	{
	  baddy = new RegularBaddy( this, pos );
	}
      
      m_baddies.push_back( std::unique_ptr<Baddy>(baddy) );
    }
}

void ArenaScene::update (float dt)
{
  sf::View old_view = m_core->window()->getView();
  m_core->window()->setView(m_main_view);

  m_player->update(dt);

  // dead ?
  {
    m_baddies.erase( std::remove_if(m_baddies.begin(), m_baddies.end(), [](auto const& baddy) {
	  return !baddy->getAlive() && !baddy->busy();
	}), m_baddies.end() );
    
    if( !m_player->getAlive() )
      {
	levelLost();
      }
  }

  // all dead ?
  {
    if( m_baddies.empty() )
      {
	newWave();
      }
  }
  
  for(auto& baddy : m_baddies)
    {
      baddy->update(dt);
    }
  
  m_ray_cache->update(dt);
  m_effect_cache->update(dt);
  m_life_bar->update(dt);
  
  m_core->window()->setView(old_view);
}


void ArenaScene::display()
{
  sf::View old_view = m_core->window()->getView();
  m_core->window()->setView(m_main_view);
  {
    m_core->window()->draw(m_background);
    m_effect_cache->display();
    m_player->display();
  
    for(auto& baddy : m_baddies)
      {
	baddy->display();
      }

    m_ray_cache->display();

  }
  m_core->window()->setView(old_view);

  m_life_bar->display();
}


void ArenaScene::buddyShot(Actor* actor, Eigen::Vector2f origin, Eigen::Vector2f target)
{
  std::pair<float, float> impact;
  Gun* gun = static_cast<Baddy*>(actor)->getGun();

  if(gun == nullptr) {return;}
  
  impact = m_core->collideCircle( origin, target, m_player->getPos(), m_player->getRadius() );

  m_effect_cache->add( origin
		       , {m_player->getRadius(), m_player->getRadius()}
		       , "fire", 16 );

  if(impact.first == -1 && impact.second == -1) {return;}

  if( !m_player->blocking()
      || m_player->getLookDir().dot( (target - origin).normalized() ) >= 0 )
    // taking damages
    {
      m_ray_cache->add( origin, {impact.first, impact.second}, gun->getRayColor() );
      
      m_effect_cache->add( {impact.first, impact.second}
			  , {m_player->getRadius(), m_player->getRadius()}
			  , "blood", 125);

      m_effect_cache->add(m_player->getPos()
			  , {m_player->getRadius() * m_core->random(1, 5)
			      , m_player->getRadius() * m_core->random(1, 5)}
			  , "blood_2", 1000*60*15);

      m_player->hit();
      m_player->hurt( actor->getAttack() );
      return;
    }

  // reflecting
  std::pair<float, float> reflect;
  Eigen::Vector2f impact_pos;
  
  impact_pos << impact.first, impact.second;
  
  Eigen::Vector2f reflect_dir = m_player->getLookDir();
  Eigen::Vector2f reflect_pos;
  reflect_pos = impact_pos + reflect_dir * (target - origin).norm();

  gun->reflect();
  m_ray_cache->add( origin, impact_pos, gun->getRayColor() );
  m_ray_cache->add( impact_pos, reflect_pos, gun->getRayColor() );

  Sword* sword = m_player->getSword();
  
  sword->reflect();

  m_effect_cache->add( impact_pos
			  , {m_player->getRadius(), m_player->getRadius()}
			  , "spark", 16);
  
  for(auto& baddy : m_baddies)
    {
      
      reflect = m_core->collideCircle( impact_pos
				       , reflect_pos
				       , baddy->getPos()
				       , baddy->getRadius() );

      // reflecting                
      if(reflect.first == -1 && reflect.second == -1) {continue;}
      
      // with damages      
      m_effect_cache->add({reflect.first, reflect.second}
			  , {baddy->getRadius(), baddy->getRadius()}
			  , "blood", 125);

      m_effect_cache->add(baddy->getPos()
			  , {baddy->getRadius() * m_core->random(1, 5)
			      , baddy->getRadius() * m_core->random(1, 5)}
			  , "blood_2", 1000*60*15);

      baddy->hurt( static_cast<Baddy*>(actor)->getAttack() );
      baddy->hit();
    }
}

void ArenaScene::playerHit(Actor* actor, Eigen::Vector2f origin, Eigen::Vector2f target)
{
  UNUSED(actor);

  for(auto& baddy : m_baddies)
    {
      std::pair<float, float> impact;
      
      impact = m_core->collideCircle(origin
				     , target
				     , baddy->getPos()
				     , baddy->getRadius() );
      
      if(impact.first == -1 && impact.second == -1) { continue; }

      // hit baddy
      m_effect_cache->add(baddy->getPos()
			  , {baddy->getRadius(), baddy->getRadius()}
			  , "blood", 125);
      
      m_player->heal( 0.5 * baddy->getAttack() );
      baddy->hit();
      baddy->hurt( m_player->getAttack() );
      
      m_effect_cache->add(baddy->getPos()
			  , {baddy->getRadius() * m_core->random(1, 5)
			      , baddy->getRadius() * m_core->random(1, 5)}
			  , "blood_2", 1000*60*15);

    }
}

void ArenaScene::newWave()
{
  m_baddies_wave++;

  if(m_baddies_wave <= m_max_baddies_wave)
    {
      addRandomBaddies(m_baddies_wave);
      return;
    }

  levelWon();
}

void ArenaScene::levelLost()
{
  m_core->sceneLost();
  m_music->stop();
}

void ArenaScene::levelWon()
{
  m_core->sceneWon();
  m_music->stop();
}
ArenaScene::~ArenaScene()
{
  
}
