/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <EffectCache.hpp>
#include <ArenaScene.hpp>

EffectCache::EffectCache(ArenaScene* arena)
  : m_arena (arena)
  , m_core ( m_arena->getCore() )
{
  
}

void EffectCache::add(Eigen::Vector2f pos
		      , Eigen::Vector2f size
		      , std::string texture_name
		      , float duration)
{
  Effect* fx = new Effect;
  fx->rect.setPosition({pos(0) - size(0)/2, pos(1) - size(1)/2});
  fx->rect.setSize({size(0), size(1)});
  fx->rect.setTexture( m_core->rsc()->getTexture(texture_name) );
  fx->duration = duration;
  m_effects.push_back( std::unique_ptr<Effect>(fx) );
}

void EffectCache::update(float dt)
{
  UNUSED(dt);
  m_effects.erase( std::remove_if(m_effects.begin(), m_effects.end(), [](auto& fx) {
	return fx->clock.getElapsedTime().asMilliseconds() > fx->duration;
      }), m_effects.end() );
}

void EffectCache::display()
{
  for(auto& fx : m_effects)
    {
      m_core->window()->draw(fx->rect);
    }
}

EffectCache::~EffectCache()
{
  
}
