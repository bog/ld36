/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


#include <WonScene.hpp>
#include <PlayerControler.hpp>

WonScene::WonScene(Core* core)
  : Scene(core)
  , m_input_delay (500)
{
  m_congrat.setFont( *m_core->rsc()->getFont("aquifer") );
  m_congrat.setCharacterSize(64);

  m_congrat.setFillColor( sf::Color(0, 50, 0) );
  m_congrat.setString("Congratulation !");

  float x = (m_core->window()->getSize().x - m_congrat.getLocalBounds().width)/2;
  float y = (m_core->window()->getSize().y - m_congrat.getLocalBounds().height)/2;

  y-= m_congrat.getLocalBounds().height * 2;
  
  m_congrat.setPosition({x, y});


  m_replay.setFont( *m_core->rsc()->getFont("aquifer") );
  m_replay.setCharacterSize( m_congrat.getCharacterSize()/2);

  m_replay.setFillColor(sf::Color::Black);
  m_replay.setString("Press <SPACEBAR> to return to the main menu");
  x = (m_core->window()->getSize().x - m_replay.getLocalBounds().width)/2;
  y += m_replay.getLocalBounds().height * 3;
  m_replay.setPosition({x, y});
}

void WonScene::update(float dt)
{
  UNUSED(dt);

  if( m_input_clock.getElapsedTime().asMilliseconds() >= m_input_delay )
    {
      if( m_core->ctrl()->enter() )
	{
	  m_input_clock.restart();
	  m_core->sceneMenu();
	}
    }
}

void WonScene::display()
{
  m_core->window()->draw(m_congrat);
  m_core->window()->draw(m_replay);
}

WonScene::~WonScene()
{
  
}
