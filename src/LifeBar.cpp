/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <LifeBar.hpp>
#include <ArenaScene.hpp>
#include <Actor.hpp>

LifeBar::LifeBar(Scene* scene, Actor* actor, sf::FloatRect rect)
  : Entity (scene)
  , m_actor (actor)
  , m_rect (rect)
{

  m_life.setFillColor(sf::Color::Red);
  
  m_life.setTexture( m_core->rsc()->getTexture("life") );
  m_bg.setTexture( m_core->rsc()->getTexture("life_bg") );
}

void LifeBar::update(float dt)
{
  UNUSED(dt);
  m_life.setPosition({ m_rect.left, m_rect.top });  
  m_life.setSize({m_rect.width, m_rect.height});
  m_bg.setPosition({m_rect.left, m_rect.top});
  m_bg.setSize({m_rect.width, m_rect.height});

  sf::Vector2f size = m_life.getSize();
  size.x = m_bg.getSize().x * m_actor->getLifeRatio();
  m_life.setSize(size);
  m_life.setTextureRect( {0, 0
	, (int) m_life.getSize().x
	, (int) m_life.getSize().y} );
}

void LifeBar::display()
{
  m_core->window()->draw(m_bg);
  m_core->window()->draw(m_life);
}

LifeBar::~LifeBar()
{
  
}
