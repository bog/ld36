/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <LostScene.hpp>
#include <PlayerControler.hpp>

LostScene::LostScene(Core* core)
  : Scene(core)
  , m_input_delay (500)
{
  m_game_over.setFont( *m_core->rsc()->getFont("aquifer") );
  m_game_over.setCharacterSize(64);

  m_game_over.setFillColor( sf::Color(50, 0, 0) );
  m_game_over.setString("Game over ...");

  float x = (m_core->window()->getSize().x - m_game_over.getLocalBounds().width)/2;
  float y = (m_core->window()->getSize().y - m_game_over.getLocalBounds().height)/2;

  y-= m_game_over.getLocalBounds().height * 2;
  
  m_game_over.setPosition({x, y});


  m_restart.setFont( *m_core->rsc()->getFont("aquifer") );
  m_restart.setCharacterSize( m_game_over.getCharacterSize()/2);

  m_restart.setFillColor(sf::Color::Black);
  m_restart.setString("Press <SPACEBAR> to return to the main menu");
  x = (m_core->window()->getSize().x - m_restart.getLocalBounds().width)/2;
  y += m_restart.getLocalBounds().height * 3;
  m_restart.setPosition({x, y});
}

void LostScene::update(float dt)
{
  UNUSED(dt);

  if( m_input_clock.getElapsedTime().asMilliseconds() >= m_input_delay )
    {
      if( m_core->ctrl()->enter() )
	{
	  m_input_clock.restart();
	  m_core->sceneMenu();
	}
    }
}

void LostScene::display()
{
  m_core->window()->draw(m_game_over);
  m_core->window()->draw(m_restart);
}

LostScene::~LostScene()
{
  
}
