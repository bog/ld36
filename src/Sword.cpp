/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Sword.hpp>
#include <Actor.hpp>
#include <ArenaScene.hpp>

Sword::Sword(Scene* scene, Actor* actor)
  : Item(scene, actor)
  , m_mode (SwordMode::Idle)
  , m_action_ratio (7)
  , m_slash (SlashStatus::Idle)
  , m_hit_enabled (true)
  , m_hit_delay(125)
{
  m_size << m_core->rsc()->getTexture("sword")->getSize().x
    , m_core->rsc()->getTexture("sword")->getSize().y;
  m_size *= 2;
  
  m_dir << 1, 0;
  
  m_shape.setTexture( m_core->rsc()->getTexture("sword") );
  m_hit_sound.setBuffer( *m_core->rsc()->getSoundBuffer("sword_hit") );
  m_reflect_sound.setBuffer( *m_core->rsc()->getSoundBuffer("sword_reflect") );
}

/*virtual*/ void Sword::update(float dt)
{
  Item::update(dt);
  
  if( sf::Mouse::isButtonPressed(sf::Mouse::Button::Right) )      
    {
      m_mode = SwordMode::Defend;
    }
  else if(m_mode == SwordMode::Defend)
    {
      m_mode = SwordMode::Idle;
    }
  
  if( sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)
      && m_hit_clock.getElapsedTime().asMilliseconds() >= m_hit_delay
      && m_hit_enabled )
    {
      m_mode = SwordMode::Slash;
      
      if(m_slash == SlashStatus::Idle)
	{
	  m_slash = SlashStatus::Up;
	}
      
      m_hit_enabled = false;
      
      m_hit_clock.restart();
    }
  else if( !sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)
	   && m_mode != SwordMode::Slash)
    {
      m_hit_enabled = true;
    }
  
  updateMode(dt);
}

void Sword::updateMode(float dt)
{ 
  switch(m_mode)
    {
    case SwordMode::Idle :
      m_dir = m_actor->getLookDir();
      break;
    case SwordMode::Slash :
      updateSlash(dt);
      break;
    case SwordMode::Defend :
      m_dir = m_core->reach( m_dir, dt * m_action_ratio
			     , m_core->angle( m_actor->getLookDir(), RAD(-75) ) );
      break;
    }
}

void Sword::updateSlash(float dt)
{
  float hit_ratio = dt * m_action_ratio;
  
  if(m_slash == SlashStatus::Up)
    {
      Eigen::Vector2f to_reach = m_core->angle( m_actor->getLookDir(), RAD(-20) );
      m_dir = m_core->reach( m_dir, hit_ratio, to_reach );
      if(m_dir == to_reach) { m_slash = SlashStatus::Down; }
    }
  else if(m_slash == SlashStatus::Down)
    {
      m_dir = m_core->reach( m_dir, hit_ratio , m_actor->getLookDir() );
      
      if( m_dir == m_actor->getLookDir() )
	{
	  hit();
	  m_slash = SlashStatus::Idle;
	  m_mode = SwordMode::Idle;
	}
    }
}

void Sword::hit()
{
  ArenaScene* scene = static_cast<ArenaScene*>(m_scene);
  scene->playerHit(m_actor, m_pos, m_pos + m_dir * m_size(0) );
  m_hit_sound.play();
}

void Sword::reflect()
{
  m_reflect_sound.play();
}

/*virtual*/ void Sword::display()
{
  m_core->window()->draw(m_shape);
}

/*virtual*/ bool Sword::busy() const
{
  return (m_hit_sound.getStatus() == sf::SoundSource::Playing
	  || m_reflect_sound.getStatus() == sf::SoundSource::Playing);
}

Sword::~Sword()
{
  
}
