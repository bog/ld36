/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <ResourceCache.hpp>

ResourceCache::ResourceCache()
{
  
}

void ResourceCache::loadTexture(std::string name, std::string path)
{
  if( m_textures.find(name) != m_textures.end() )
    {
      throw std::runtime_error("Texture "
			       + name
			       + " already exists !");
    }

  std::unique_ptr<sf::Texture> t = std::make_unique<sf::Texture>();
  t->loadFromFile(std::string(DATA_PATH) + path);
  
  m_textures.insert( std::make_pair(name, std::move(t) ) );
}

sf::Texture* ResourceCache::getTexture(std::string name)
{
  if( m_textures.find(name) == m_textures.end() )
    {
      throw std::runtime_error("Texture "
			       + name
			       + " did not exists !");
    }
  
  return m_textures[name].get();
}


void ResourceCache::loadSoundBuffer(std::string name, std::string path)
{
  if( m_sound_buffers.find(name) != m_sound_buffers.end() )
    {
      throw std::runtime_error("Sound buffer "
			       + name
			       + " already exists !");
    }

  std::unique_ptr<sf::SoundBuffer> sb = std::make_unique<sf::SoundBuffer>();
  sb->loadFromFile(std::string(DATA_PATH) + path);
  
  m_sound_buffers.insert( std::make_pair(name, std::move(sb) ) );
}

sf::SoundBuffer* ResourceCache::getSoundBuffer(std::string name)
{
  if( m_sound_buffers.find(name) == m_sound_buffers.end() )
    {
      throw std::runtime_error("Sound buffer "
			       + name
			       + " did not exists !");
    }
  
  return m_sound_buffers[name].get();
}

void ResourceCache::loadMusic(std::string name, std::string path)
{
  if( m_musics.find(name) != m_musics.end() )
    {
      throw std::runtime_error("Music "
			       + name
			       + " already exists !");
    }

  std::unique_ptr<sf::Music> m = std::make_unique<sf::Music>();
  m->openFromFile(std::string(DATA_PATH) + path);
  
  m_musics.insert( std::make_pair(name, std::move(m) ) );
}

sf::Music* ResourceCache::getMusic(std::string name)
{
  if( m_musics.find(name) == m_musics.end() )
    {
      throw std::runtime_error("Music "
			       + name
			       + " did not exists !");
    }
  
  return m_musics[name].get();
}

void ResourceCache::loadFont(std::string name, std::string path)
{
    if( m_fonts.find(name) != m_fonts.end() )
    {
      throw std::runtime_error("Font "
			       + name
			       + " already exists !");
    }

  std::unique_ptr<sf::Font> f = std::make_unique<sf::Font>();
  f->loadFromFile(std::string(DATA_PATH) + path);
  
  m_fonts.insert( std::make_pair(name, std::move(f) ) );
}

sf::Font* ResourceCache::getFont(std::string name)
{
   if( m_fonts.find(name) == m_fonts.end() )
    {
      throw std::runtime_error("Font "
			       + name
			       + " did not exists !");
    }
  
  return m_fonts[name].get();
}

ResourceCache::~ResourceCache()
{
  
}
