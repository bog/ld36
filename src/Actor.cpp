#include <Actor.hpp>

Actor::Actor(Scene* scene, float max_life, float attack)
  : Entity(scene)
  , m_radius (32)
  , m_max_velocity (m_radius * 6)
  , m_velocity (0)
  , m_max_life (max_life)
  , m_life (max_life)
  , m_alive (true)
  , m_attack(attack)
{
  m_dir << 0, 1;
  m_look_dir = m_dir;
}

void Actor::update (float dt)
{
  UNUSED(dt);
  if( !m_alive ) { return; }
    
  m_angle = acos( m_look_dir.dot( Eigen::Vector2f(0, 1) ) );
  if( m_look_dir(0) > 0 ) { m_angle *= -1; }

  m_shape.setRadius(m_radius);
  m_shape.setOrigin({m_radius, m_radius});
  m_shape.setPosition({m_pos(0), m_pos(1)});
  m_shape.setRotation( DEG(m_angle) );
  
  m_bg.setRadius(m_radius/10);
  m_bg.setFillColor( sf::Color(255, 0, 0, 255) );
  m_bg.setPosition({m_pos(0) - m_radius/10, m_pos(1) - m_radius/10});
}

void Actor::display()
{
  m_core->window()->draw(m_shape);
}

void Actor::hurt(float damage)
{
  damage = abs(damage);
  m_life -= damage;
  if(m_life <= 0){ m_life = 0; m_alive = false; }
}

Actor::~Actor()
{
  
}
