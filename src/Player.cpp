/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Player.hpp>
#include <Sword.hpp>
#include <PlayerControler.hpp>

Player::Player(Scene* scene)
  : Actor(scene, 100, 75)
{
  m_shape.setTexture( m_core->rsc()->getTexture("player") );
  m_pos << 256, 256;
  m_sword = std::make_unique<Sword>(m_scene, this);
  m_max_velocity = m_radius * 8;
  
  m_hit_sound.setBuffer( *m_core->rsc()->getSoundBuffer("baddy_hit") );
}

void Player::updateInputs(float dt)
{
  // mouse
  {
    // look
    Eigen::Vector2f mouse_pos;

    {
      sf::Vector2i tmp = sf::Mouse::getPosition( *m_core->window() );
      sf::Vector2f tmp2 = m_core->window()->mapPixelToCoords(tmp);
      mouse_pos << tmp2.x, tmp2.y;
    }
    
    m_look_dir = (mouse_pos - m_pos).normalized();

    // walk
    {
      Eigen::Vector2f next_dir = {0, 0};
      bool walked = false;
      float dt_test = dt * 2000;
      
      if( (m_pos - mouse_pos).norm() > m_radius )
	{
	  if( m_core->ctrl()->up() )
	    {
	      next_dir += Eigen::Vector2f(0, -1);
	      walked = true;
	    }

	  if( m_core->ctrl()->down() )
	    {
	      next_dir += Eigen::Vector2f(0, 1);
	      walked = true;
	    }

	  if( m_core->ctrl()->left() )
	    {
	      next_dir += Eigen::Vector2f(-1, 0);	      
	      walked = true;
	    }

	  if( m_core->ctrl()->right() )
	    {
	      next_dir += Eigen::Vector2f(1, 0);
	      walked = true;
	    }
		  
	  next_dir.normalized();

	  m_dir = next_dir;
	  
	  m_velocity = m_core->reach(m_velocity, dt_test, m_max_velocity);
	}
      
      if( !walked ) { m_velocity = m_core->reach(m_velocity, dt_test, 0); }
    }
  }  
}

void Player::update (float dt)
{
  updateInputs(dt);
  Actor::update(dt);

  if( !blocking() )
    {
      m_pos += m_velocity * m_dir * dt;
    }
  else
    {
      m_pos += m_velocity/8.0 * m_dir * dt;
    }
  
  // screen collision
  {
  if(m_pos(0) - m_radius < 0)
    {
      m_pos(0) = m_radius;
    }
  if(m_pos(0) + m_radius >= m_core->window()->getView().getSize().x)
    {
      m_pos(0) = m_core->window()->getView().getSize().x - m_radius;
    }
  if(m_pos(1) - m_radius < 0)
    {
      m_pos(1) = m_radius;
    }
  if(m_pos(1) + m_radius >= m_core->window()->getView().getSize().y)
    {
      m_pos(1) = m_core->window()->getView().getSize().y - m_radius;
    }
  }
  
  m_sword->update(dt);
}

void Player::display()
{
  Actor::display();
  m_sword->display();
}

void Player::heal(float h)
{
  m_life += h;
  if(m_life > 100)
    {
      m_life = 100;
    }
}

/*virtual*/ bool Player::busy() const
{
  return m_sword->busy()
    || m_hit_sound.getStatus() == sf::SoundSource::Playing;
}

void Player::hit()
{
  m_hit_sound.play();
}

Player::~Player()
{
  
}
