/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <PlayerControler.hpp>

PlayerControler::PlayerControler()
{
  
}

bool PlayerControler::enter()
{
  return sf::Keyboard::isKeyPressed(sf::Keyboard::Space);
}

bool PlayerControler::back()
{
  return sf::Keyboard::isKeyPressed(sf::Keyboard::Escape);
}

bool PlayerControler::help()
{
  return sf::Keyboard::isKeyPressed(sf::Keyboard::H);
}

bool PlayerControler::up()
{
  return sf::Keyboard::isKeyPressed(sf::Keyboard::Z)
    || sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
}

bool PlayerControler::down()
{
  return sf::Keyboard::isKeyPressed(sf::Keyboard::S)
    || sf::Keyboard::isKeyPressed(sf::Keyboard::Down);
}

bool PlayerControler::left()
{
  return sf::Keyboard::isKeyPressed(sf::Keyboard::Q)
    || sf::Keyboard::isKeyPressed(sf::Keyboard::Left);
}

bool PlayerControler::right()
{
  return sf::Keyboard::isKeyPressed(sf::Keyboard::D)
    || sf::Keyboard::isKeyPressed(sf::Keyboard::Right);
}

PlayerControler::~PlayerControler()
{
  
}
