/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Core.hpp>
#include <ArenaScene.hpp>
#include <LostScene.hpp>
#include <WonScene.hpp>
#include <MenuScene.hpp>
#include <TutorialScene.hpp>
#include <PlayerControler.hpp>

Core::Core(sf::RenderWindow* window)
  : m_window (window)
  , m_current_scene (nullptr)
{
  srand(time(NULL));
  m_rsc = std::make_unique<ResourceCache>();
  m_ctrl = std::make_unique<PlayerControler>();
  loadRsc();
  sceneMenu();  
}

void Core::loadRsc()
{
  m_rsc->loadTexture("player", "/sprites/player.png");
  m_rsc->loadTexture("regular_baddy", "/sprites/regular_baddy.png");
  m_rsc->loadTexture("regular_gun", "/sprites/regular_gun.png");
  m_rsc->loadTexture("light_baddy", "/sprites/light_baddy.png");
  m_rsc->loadTexture("light_gun", "/sprites/light_gun.png");
  m_rsc->loadTexture("sniper_baddy", "/sprites/sniper_baddy.png");
  m_rsc->loadTexture("sniper_gun", "/sprites/sniper_gun.png");
  m_rsc->loadTexture("sword", "/sprites/sword.png");
  
  m_rsc->loadTexture("blood", "/sprites/blood.png");
  m_rsc->loadTexture("blood_2", "/sprites/blood_2.png");
  m_rsc->loadTexture("spark", "/sprites/spark.png");
  m_rsc->loadTexture("fire", "/sprites/fire.png");
  m_rsc->loadTexture("life", "/sprites/life.png");
  m_rsc->loadTexture("life_bg", "/sprites/life_bg.png");
  
  m_rsc->loadTexture("menu_bg", "/backgrounds/menu_bg.png");
  m_rsc->loadTexture("tutorial_bg", "/backgrounds/tutorial_bg.png");
  m_rsc->loadTexture("arena_bg", "/backgrounds/arena_bg.png");
  
  m_rsc->loadSoundBuffer("regular_gun", "/sounds/regular_gun.wav");
  m_rsc->loadSoundBuffer("regular_reflect", "/sounds/regular_reflect.wav");
  m_rsc->loadSoundBuffer("light_gun", "/sounds/light_gun.wav");
  m_rsc->loadSoundBuffer("light_reflect", "/sounds/light_reflect.wav");
  m_rsc->loadSoundBuffer("sniper_gun", "/sounds/sniper_gun.wav");
  m_rsc->loadSoundBuffer("sniper_reflect", "/sounds/sniper_reflect.wav");
  m_rsc->loadSoundBuffer("sword_hit", "/sounds/sword_hit.ogg");
  m_rsc->loadSoundBuffer("sword_reflect", "/sounds/sword_reflect.wav");
  m_rsc->loadSoundBuffer("baddy_hit", "/sounds/baddy_hit.ogg");
  
  m_rsc->loadMusic("menu", "/musics/menu.ogg");
  m_rsc->loadMusic("tutorial", "/musics/tutorial.ogg");
  m_rsc->loadMusic("arena", "/musics/arena.ogg");
  
  m_rsc->loadFont("berry_rotunda", "/fonts/BerryRotunda.ttf");
  m_rsc->loadFont("chalk_line_outline", "/fonts/ChalkLineOutline.ttf");
  m_rsc->loadFont("aquifer", "/fonts/Aquifer.ttf");
}

void Core::sceneArena()
{
  m_arena_scene.reset( new ArenaScene(this) );
  m_current_scene = m_arena_scene.get();
}

void Core::sceneLost()
{
  m_lost_scene.reset( new LostScene(this) );
  m_current_scene = m_lost_scene.get();
}

void Core::sceneWon()
{
  m_won_scene.reset( new WonScene(this) );
  m_current_scene = m_won_scene.get();
}

void Core::sceneMenu()
{
  m_menu_scene.reset( new MenuScene(this) );
  m_current_scene = m_menu_scene.get();
}

void Core::sceneTutorial()
{
  m_tutorial_scene.reset( new TutorialScene(this) );
  m_current_scene = m_tutorial_scene.get();
}


void Core::update(float dt)
{
  if(m_current_scene) { m_current_scene->update(dt); }
}

void Core::display()
{
  if(m_current_scene) { m_current_scene->display(); }
}

void Core::run()
{
  sf::Event event;
  sf::Clock loop_clock;
  float dt = 0;
  
  while( m_window->isOpen() )
    {
      while( m_window->pollEvent(event) )
	{
	  if( event.type == sf::Event::Closed )
	    {
	      m_window->close();
	    }
	}

      update(dt);
      
      m_window->clear(sf::Color(4, 139, 154));
      display();
      m_window->display();

      dt = loop_clock.getElapsedTime().asSeconds();
      loop_clock.restart();
    }
}

float Core::reach(float current, float dt, float goal) const
{
  if( current < goal )
    {
      float next = current + dt;
      
      if(next > goal)
	{
	  return goal;
	}
      
      return next;
    }
  else if(current > goal)
    {
      float next = current - dt;
      
      if(next < goal)
	{
	  return goal;
	}

      return next;
    }
  
  return goal;
}

Eigen::Vector2f Core::reach(Eigen::Vector2f current
			    , float dt
			    , Eigen::Vector2f goal) const
{
  Eigen::Vector2f next;

  next << reach( current(0), dt, goal(0) )
    , reach( current(1), dt, goal(1) );

  return next;
}

Eigen::Vector2f Core::projection(Eigen::Vector2f point, Eigen::Vector2f vec)
{
  UNUSED(point);
  UNUSED(vec);
  return {0, 0}; // TODO
}

Eigen::Vector2f Core::angle(Eigen::Vector2f vec, float rad)
{
  Eigen::Matrix2f rot;
  rot << cos(rad), -sin(rad)
    , sin(rad), cos(rad);
  
  return rot * vec;
}

std::pair<float, float> Core::collideCircle (Eigen::Vector2f A
					 , Eigen::Vector2f B
					 , Eigen::Vector2f center
					 , float radius)
{
  Eigen::Vector2f D = B - A;

  for(float t=0; t<1; t+= 0.0025)
    {
      Eigen::Vector2f point = A + D*t;
      if( (center - point).norm() < radius )
	{
	  return {point(0), point(1)};
	}
    }
  
  return {-1, -1};
}
  
Core::~Core()
{
  
}
