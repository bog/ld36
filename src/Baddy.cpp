/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Baddy.hpp>
#include <ArenaScene.hpp>
#include <LifeBar.hpp>

Baddy::Baddy(Scene* scene
	     , Eigen::Vector2f pos
	     , std::string texture_name
	     , float maxlife
	     , float attack
	     , float shot_radius_dist)
  : Actor(scene, maxlife, attack)
  , m_shot_dist(m_radius * shot_radius_dist)
  , m_dt_ratio (1500)
{
  m_look_dir <<1, 0;
  m_dir = m_look_dir;
  m_velocity = 0;
  m_max_velocity = m_radius * 3;
  m_pos = pos;
  m_shape.setTexture( m_core->rsc()->getTexture(texture_name) );
  m_gun = nullptr;
  m_life_bar = std::make_unique<LifeBar>(m_scene, this, sf::FloatRect(0, 0, 0, 0));
  m_initial_delay = m_core->random(0, 1000) + 1;
  m_hit_sound.setBuffer( *m_core->rsc()->getSoundBuffer("baddy_hit") );
  m_hit_sound.setVolume(120);
}

void Baddy::walkInside(float dt)
{
  Eigen::Vector2f win_size;
  win_size << m_core->window()->getView().getSize().x
    , m_core->window()->getView().getSize().y;

  Eigen::Vector2f center;
  center << win_size(0)/2, win_size(1)/2;

  if( m_pos(0) - m_radius < 0
      || m_pos(0) + m_radius >= win_size(0)
      || m_pos(1) - m_radius < 0
      || m_pos(1) + m_radius >= win_size(1) )
    {
      m_dir = m_core->reach(m_dir, dt * m_dt_ratio, (center - m_pos).normalized());
      m_look_dir = m_dir;
      m_velocity = m_core->reach(m_velocity, m_dt_ratio * dt, m_max_velocity);      
    }
  else
    {
      walkAway(dt);
    }
}

void Baddy::walkAway(float dt)
{    
  ArenaScene* scene = static_cast<ArenaScene*>(m_scene);

    if( scene->getBaddiesCount() < 2 )
    {
      walkPlayer(dt);
      return;
    }
    
  Baddy* other = nullptr;
  Eigen::Vector2f to_baddy;
  
  for(size_t i=0; i<scene->getBaddiesCount(); i++)
    {
      Baddy* baddy = scene->getBaddy(i);
      if(baddy == this) { continue; }
      to_baddy = baddy->getPos() - m_pos;
      
      if( to_baddy.norm() <= baddy->getRadius() + m_radius*3 )
	{
	  other = baddy;
	  break;
	}
    }

  if(other == nullptr)
    {
      walkPlayer(dt);
      return;
    }

  // on it
  while(to_baddy(0) == 0 && to_baddy(1) == 0)
    {
      to_baddy(0) = m_core->random(-5, 5);
      to_baddy(1) = m_core->random(-5, 5);
    }

  m_dir = m_core->reach(m_dir, dt * m_dt_ratio, (-1 * to_baddy).normalized() );  
  m_velocity = m_core->reach(m_velocity, m_dt_ratio * dt, m_max_velocity);
}

void Baddy::walkPlayer(float dt)
{
  ArenaScene* scene = static_cast<ArenaScene*>(m_scene);
  Player* player = scene->getPlayer();

  Eigen::Vector2f gun_offset = Eigen::Vector2f::Zero();
  Eigen::Vector2f to_player = player->getPos() - m_pos;
  
  if( m_gun != nullptr)
    {
      gun_offset = m_gun->getOffset();
      to_player = ( ( player->getPos() - gun_offset.normalized() * m_radius)
				- m_pos);
    }
    
  m_look_dir = m_core->reach(m_look_dir, dt * 5, to_player.normalized() );
  m_dir = m_look_dir;
  
  if( to_player.norm() > player->getRadius() + m_radius + m_shot_dist )
    {
      m_velocity = m_core->reach(m_velocity, m_dt_ratio * dt, m_max_velocity);
    }
  else
    {
      m_velocity = m_core->reach(m_velocity, m_dt_ratio * dt, 0);
      
      if(m_gun != nullptr
	 && m_initial_clock.getElapsedTime().asMilliseconds() > m_initial_delay )
	{
	  m_gun->shot();
	}
    }
}

/*virtual*/ void  Baddy::update(float dt)
{
  if( !m_alive ) { return; }

  Actor::update(dt);
  m_pos += m_velocity * m_dir * dt;

  if(m_gun != nullptr)
    {
      m_gun->update(dt);
    }
  
  //walkAway(dt);
  walkInside(dt);

  // lifebar
  {
    float updown;

    updown = (m_look_dir(1) < 0)
      ?
      m_pos(1) + m_radius + m_radius/4.0
      :
      m_pos(1) - m_radius - m_radius/4.0;
    
    m_life_rect = sf::FloatRect (m_pos(0) - m_radius
				 , updown
				 , m_radius * 2
				 , m_radius/4.0);
  
    m_life_bar->setRect(m_life_rect);
    m_life_bar->update(dt);
  }
}

/*virtual*/ void Baddy::display()
{
  Actor::display();
  if(m_gun != nullptr)
    {
      m_gun->display();
    }
  m_life_bar->display();
}

void Baddy::hit()
{
  if(m_alive)
    {
      m_hit_sound.play();
    }
}

/*virtual*/ bool Baddy::busy() const
{
  return ( m_gun != nullptr && m_gun->busy() )
    || m_hit_sound.getStatus() == sf::SoundSource::Playing;
}

Baddy::~Baddy()
{
  
}
