/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <RayCache.hpp>
#include <ArenaScene.hpp>

RayCache::RayCache(ArenaScene* arena)
  : m_arena(arena)
  , m_core ( m_arena->getCore() )
  , m_ray_default_duration (64)
{
  
}

void RayCache::add(Eigen::Vector2f x, Eigen::Vector2f y, sf::Color color)
{
  add(x, y, color, m_ray_default_duration);
}

void RayCache::add(Eigen::Vector2f x, Eigen::Vector2f y, sf::Color color, float duration)
{
  Ray* ray = new Ray;

  ray->points.resize(2);
  ray->points.setPrimitiveType(sf::Lines);
  
  ray->points[0].position = {x(0), x(1)};
  ray->points[1].position = {y(0), y(1)};

  ray->points[0].color = color;
  ray->points[1].color = color;
    
  ray->duration = duration;

  m_rays.push_back( std::unique_ptr<Ray>(ray) );
}

void RayCache::update(float dt)
{
  UNUSED(dt);
  m_rays.erase( std::remove_if(m_rays.begin(), m_rays.end(), [](auto const& ray){
	return ray->clock.getElapsedTime().asMilliseconds() >= ray->duration;
      }), m_rays.end() );
}

void RayCache::display()
{
  for(auto& ray : m_rays)
    {
      m_core->window()->draw( ray->points );
    }
}

RayCache::~RayCache()
{
  
}
