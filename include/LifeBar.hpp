/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef LIFEBAR_HPP
#define LIFEBAR_HPP
#include <iostream>
#include <Entity.hpp>

class Actor;

class LifeBar : public Entity
{
 public:
  explicit LifeBar(Scene* scene, Actor* actor, sf::FloatRect rect);
  virtual ~LifeBar();

  void update(float dt) override;
  void display() override;

  inline void setRect(sf::FloatRect r) { m_rect = r; }
  
 protected:
  Actor* m_actor;
  sf::FloatRect m_rect;
  sf::RectangleShape m_bg;
  sf::RectangleShape m_life;
  
 private:
  LifeBar( LifeBar const& lifebar ) = delete;
  LifeBar& operator=( LifeBar const& lifebar ) = delete;
};

#endif
