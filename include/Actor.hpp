/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef ACTOR_HPP
#define ACTOR_HPP
#include <iostream>
#include <Entity.hpp>

class Actor : public Entity
{
 public:
  explicit Actor(Scene* scene, float max_life, float attack);
  virtual ~Actor();

  void update (float dt) override;
  void display() override;

  void hurt(float damage);
  
  // services
  virtual inline bool getAlive() const { return m_alive; }
  virtual inline float getAttack() const { return m_attack; }
  virtual inline Eigen::Vector2f getPos() const { return m_pos; }
  virtual inline Eigen::Vector2f getDir() const { return m_dir; }
  virtual inline Eigen::Vector2f getLookDir() const { return m_look_dir; }
  virtual inline float getRadius() const { return m_radius; }
  virtual inline float getAngle() const { return m_angle; }
  virtual inline float getLifeRatio() const { return m_life/m_max_life; }
  virtual inline float getMaxLife() const { return m_max_life; }
  
 protected:
  sf::CircleShape m_shape;
  sf::CircleShape m_bg;
  float m_radius;

  float m_angle;
  float m_max_velocity;
  float m_velocity;  
  Eigen::Vector2f m_pos;
  Eigen::Vector2f m_dir;
  Eigen::Vector2f m_look_dir;

  float m_max_life;
  float m_life;
  bool m_alive;

  float m_attack;
  
 private:
  Actor( Actor const& actor ) = delete;
  Actor& operator=( Actor const& actor ) = delete;
};

#endif
