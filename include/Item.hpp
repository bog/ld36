/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef ITEM_HPP
#define ITEM_HPP
#include <iostream>
#include <Entity.hpp>

class Actor;

class Item : public Entity
{
 public:
  explicit Item(Scene* scene, Actor* actor);
  virtual ~Item();

  virtual void update(float dt) override;
  virtual void display() override;

  void setAngle(float angle);
  // services
  virtual inline Eigen::Vector2f getPos() const { return m_pos; }
  virtual inline Eigen::Vector2f getSize() const { return m_size; }
  virtual inline Eigen::Vector2f getOffset() const { return m_offset; }
  
 protected:
  Actor* m_actor;
  float m_angle;
  sf::RectangleShape m_shape;
  
  Eigen::Vector2f m_pos;
  Eigen::Vector2f m_dir;
  Eigen::Vector2f m_size;
  Eigen::Vector2f m_offset;
 private:
  Item( Item const& item ) = delete;
  Item& operator=( Item const& item ) = delete;
};

#endif
