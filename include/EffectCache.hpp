/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef EFFECTCACHE_HPP
#define EFFECTCACHE_HPP
#include <iostream>
#include <memory>
#include <vector>
#include <eigen3/Eigen/Dense>
#include <SFML/Graphics.hpp>

class ArenaScene;
class Core;

struct Effect
{
  Effect() = default;
  sf::RectangleShape rect;
  float duration;
  sf::Clock clock;
};

class EffectCache
{
 public:
  explicit EffectCache(ArenaScene* arena);
  virtual ~EffectCache();
  
  void add(Eigen::Vector2f pos
	   , Eigen::Vector2f size
	   , std::string texture_name
	   , float duration);

  void update(float dt);
  void display();
 protected:
  ArenaScene* m_arena;
  Core* m_core;
  std::vector< std::unique_ptr<Effect> > m_effects;
  
 private:
  EffectCache( EffectCache const& effectcache ) = delete;
  EffectCache& operator=( EffectCache const& effectcache ) = delete;
};

#endif
