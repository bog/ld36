/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GUN_HPP
#define GUN_HPP
#include <iostream>
#include <Item.hpp>

class Gun : public Item
{
 public:
  explicit Gun(Scene* scene
	       , Actor* actor
	       , unsigned int shot_delay
	       , sf::Color rayline_color
	       , std::string texture_name
	       , std::string sound_name
	       , std::string sound_reflect_name);
  virtual ~Gun();

  virtual void update(float dt) override;

  virtual void display() override;

  virtual void shot();

  virtual void reflect();

  inline sf::Color getRayColor() const { return m_rayline_color; }

  virtual bool busy() const;
 protected:
  sf::CircleShape m_shot_point_shape;

  float m_shot_dist;
  sf::Clock m_shot_clock;
  float m_shot_delay;  

  Eigen::Vector2f m_shot_point;

  sf::Color m_rayline_color;
  sf::Sound m_shot_sound;
  sf::Sound m_reflect_sound; 
  
 private:
  Gun( Gun const& gun ) = delete;
  Gun& operator=( Gun const& gun ) = delete;
};

#endif
