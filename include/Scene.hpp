/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef SCENE_HPP
#define SCENE_HPP
#include <iostream>
#include <Core.hpp>

class Scene
{
 public:
  explicit Scene(Core* core);
  virtual ~Scene();

  virtual void update(float dt) = 0;
  virtual void display() = 0;

  inline Core* getCore() const { return m_core; }
  
 protected:
  Core* m_core;
  
 private:
  Scene( Scene const& scene ) = delete;
  Scene& operator=( Scene const& scene ) = delete;
};

#endif
