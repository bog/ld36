/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef ARENASCENE_HPP
#define ARENASCENE_HPP
#include <iostream>
#include <Scene.hpp>
#include <Player.hpp>
#include <Baddy.hpp>
#include <Gun.hpp>
#include <LifeBar.hpp>
#include <RayCache.hpp>
#include <EffectCache.hpp>

class ArenaScene : public Scene
{
 public:
  explicit ArenaScene(Core* core);
  virtual ~ArenaScene();

  void addRandomBaddies(size_t n);
  
  void update (float dt) override;
  void updateRayline(float dt);
  void display() override;

  void buddyShot(Actor* actor, Eigen::Vector2f origin, Eigen::Vector2f target);
  void playerHit(Actor* actor, Eigen::Vector2f origin, Eigen::Vector2f target);

  void newWave();
  void levelLost();
  void levelWon();
  
  // services
  inline unsigned int getBaddiesCount() const { return m_baddies.size(); }
  inline Baddy* getBaddy(size_t index)
  {
    if( index >= getBaddiesCount() )
      {
	throw std::invalid_argument("Baddy number "
				    + std::to_string(index)
				    + " does not exists.");
      }
    
    return m_baddies[index].get();
  }
  
  inline Player* getPlayer() const { return m_player.get(); }
  inline RayCache* getRayCache() const { return m_ray_cache.get(); }
  inline EffectCache* sfx() const { return m_effect_cache.get(); }
  
 protected:
  std::unique_ptr<Player> m_player;
  std::vector<std::unique_ptr<Baddy>> m_baddies;
  sf::View m_main_view;
    
  std::unique_ptr<LifeBar> m_life_bar;
  std::unique_ptr<RayCache> m_ray_cache;
  std::unique_ptr<EffectCache> m_effect_cache;
  
  unsigned int m_baddies_wave;
  unsigned const int m_max_baddies_wave;

  sf::RectangleShape m_background;

  sf::Music* m_music;
  
 private:
  ArenaScene( ArenaScene const& arenascene ) = delete;
  ArenaScene& operator=( ArenaScene const& arenascene ) = delete;
};

#endif
