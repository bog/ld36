/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef TUTORIALSCENE_HPP
#define TUTORIALSCENE_HPP
#include <iostream>
#include <Scene.hpp>

class TutorialScene : public Scene
{
 public:
  explicit TutorialScene(Core* core);
  virtual ~TutorialScene();

  void update(float dt) override;
  void display() override;
  
 protected:
  sf::Clock m_input_clock;
  float m_input_delay;
  sf::RectangleShape m_bg;
  sf::Music *m_music;
  
 private:
  TutorialScene( TutorialScene const& tutorialscene ) = delete;
  TutorialScene& operator=( TutorialScene const& tutorialscene ) = delete;
};

#endif
