/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef RAYCACHE_HPP
#define RAYCACHE_HPP
#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>
#include <eigen3/Eigen/Dense>

class ArenaScene;
class Core;

struct Ray
{
  sf::VertexArray points;
  sf::Clock clock;
  float duration;
};

class RayCache
{
 public:
  explicit RayCache(ArenaScene* arena);
  virtual ~RayCache();

  void update(float dt);
  void display();

  void add(Eigen::Vector2f x, Eigen::Vector2f y, sf::Color color, float duration);
  void add(Eigen::Vector2f x, Eigen::Vector2f y, sf::Color color);
  
 protected:
  ArenaScene* m_arena;
  Core* m_core;
  std::vector< std::unique_ptr<Ray> > m_rays;
  float const m_ray_default_duration;
 private:
  RayCache( RayCache const& raycache ) = delete;
  RayCache& operator=( RayCache const& raycache ) = delete;
};

#endif
