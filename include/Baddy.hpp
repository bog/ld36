/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef BADDY_HPP
#define BADDY_HPP
#include <iostream>
#include <Actor.hpp>
#include <Gun.hpp>
#include <SFML/Graphics.hpp>

class LifeBar;

class Baddy : public Actor
{
 public:
  explicit Baddy(Scene* scene
		 , Eigen::Vector2f pos
		 , std::string texture_name
		 , float maxlife
		 , float attack
		 , float shot_radius_dist);
  virtual ~Baddy();

  virtual void  update(float dt) override;
  virtual void display() override;
  virtual bool busy() const;
  void walkAway(float dt);
  void walkPlayer(float dt);
  void walkInside(float dt);
  
  void hit();
  //services
  inline Gun* getGun() const { return m_gun.get(); }
  
 protected:
  float m_shot_dist;
  std::unique_ptr<Gun> m_gun;
  std::unique_ptr<LifeBar> m_life_bar;
  sf::FloatRect m_life_rect;
  float m_dt_ratio;
  
  float m_initial_delay;
  sf::Clock m_initial_clock;
  sf::Sound m_hit_sound;
 private:
  Baddy( Baddy const& baddy ) = delete;
  Baddy& operator=( Baddy const& baddy ) = delete;
};

struct RegularBaddy : public Baddy
{
  explicit RegularBaddy(Scene* scene, Eigen::Vector2f pos)
    : Baddy(scene
	    , pos
	    , "regular_baddy"
	    , 50 /*life*/
	    , 10 /*attack*/
	    , 10 /*shotdist*/)
    {
      m_gun = std::make_unique<Gun>(scene
				    , this
				    , 1000
				    , sf::Color::Red
				    , "regular_gun"
				    , "regular_gun"
				    , "regular_gun");
    }
};

struct LightBaddy : public Baddy
{
  explicit LightBaddy(Scene* scene, Eigen::Vector2f pos)
    : Baddy(scene
	    , pos
	    , "light_baddy"
	    , 64 /*life*/
	    , 1 /*attack*/
	    , 8 /*shotdist*/)

    {
      m_gun = std::make_unique<Gun>(scene
				    , this
				    , 125
				    , sf::Color(0, 0, 64)
				    , "light_gun"
				    , "light_gun"
				    , "light_gun");
    }
};

struct SniperBaddy : public Baddy
{
  explicit SniperBaddy(Scene* scene, Eigen::Vector2f pos)
    : Baddy(scene
	    , pos
	    , "sniper_baddy"
	    , 25 /*life*/
	    , 300 /*attack*/
	    , 16 /*shotdist*/)

    {
      m_gun = std::make_unique<Gun>(scene
				    , this
				    , 5000
				    , sf::Color::Yellow
				    , "sniper_gun"
				    , "sniper_gun"
				    , "sniper_gun");
    }
};

#endif
