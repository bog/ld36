/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <iostream>
#include <Actor.hpp>
#include <Sword.hpp>

class Player : public Actor
{
 public:
  explicit Player(Scene* scene);
  virtual ~Player();

  void update (float dt) override;
  void display() override;

  void updateInputs(float dt);
  virtual bool busy() const;
  void heal(float h);
  void hit();
  
  // services
  inline bool blocking() const { return m_sword->getMode() == SwordMode::Defend; }
  inline Sword* getSword() const { return m_sword.get(); }
 protected:
  std::unique_ptr<Sword> m_sword;
  sf::Sound m_hit_sound;
  
 private:
  Player( Player const& player ) = delete;
  Player& operator=( Player const& player ) = delete;
};

#endif
