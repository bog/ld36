/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef SWORD_HPP
#define SWORD_HPP
#include <iostream>
#include <Item.hpp>

enum class SwordMode
  {
    Idle
      , Slash
      , Defend
  };

enum class SlashStatus
  {
    Idle
      , Up
      , Down
  };

class Sword : public Item
{
 public:
  explicit Sword(Scene* scene, Actor* actor);
  virtual ~Sword();

  virtual void update(float dt) override;
  void updateMode(float dt);
  void updateSlash(float dt);
  
  virtual void display() override;

  void hit();
  void reflect();
  virtual bool busy() const;
  
  //services
  inline SwordMode getMode() const { return m_mode; }
  
 protected:
  SwordMode m_mode;
  float m_anim_angle;
  float m_action_ratio;
  SlashStatus m_slash;

  bool m_hit_enabled;
  float m_hit_delay;
  sf::Clock m_hit_clock;
  sf::Sound m_hit_sound;
  sf::Sound m_reflect_sound;
  
 private:
  Sword( Sword const& sword ) = delete;
  Sword& operator=( Sword const& sword ) = delete;
};

#endif
