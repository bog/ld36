/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef RESOURCECACHE_HPP
#define RESOURCECACHE_HPP
#include <iostream>
#include <memory>
#include <unordered_map>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#define DATA_PATH "../assets"

class ResourceCache
{
 public:
  explicit ResourceCache();
  virtual ~ResourceCache();

  void loadTexture(std::string name, std::string path);
  sf::Texture* getTexture(std::string name);

  void loadSoundBuffer(std::string name, std::string path);
  sf::SoundBuffer* getSoundBuffer(std::string name);

  void loadMusic(std::string name, std::string path);
  sf::Music* getMusic(std::string name);

  void loadFont(std::string name, std::string path);
  sf::Font* getFont(std::string name);

 protected:
  std::unordered_map<std::string, std::unique_ptr<sf::Texture>> m_textures;
  std::unordered_map<std::string, std::unique_ptr<sf::SoundBuffer>> m_sound_buffers;
  std::unordered_map<std::string, std::unique_ptr<sf::Music>> m_musics;
  std::unordered_map<std::string, std::unique_ptr<sf::Font>> m_fonts;
  
 private:
  ResourceCache( ResourceCache const& resourcecache ) = delete;
  ResourceCache& operator=( ResourceCache const& resourcecache ) = delete;
};

#endif
