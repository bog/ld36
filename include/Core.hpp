/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CORE_HPP
#define CORE_HPP
#include <iostream>
#include <memory>
#include <cmath>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <eigen3/Eigen/Dense>
#include <ResourceCache.hpp>

#define UNUSED(x) ( (void)(x) )
#define RAD(deg) ( (M_PI * deg)/180 )
#define DEG(rad) ( (360 * rad)/(2*M_PI) )

class Scene;
class ArenaScene;
class LostScene;
class WonScene;
class MenuScene;
class TutorialScene;
class PlayerControler;

class Core
{
 public:
  explicit Core(sf::RenderWindow* window);
  virtual ~Core();

  void update(float dt);
  void display();
  void run();

  // services
  inline sf::RenderWindow* window() const { return m_window; }
  inline ResourceCache* rsc() const { return m_rsc.get(); }
  inline PlayerControler* ctrl() const { return m_ctrl.get(); }
  float reach(float current, float dt, float goal) const;
  Eigen::Vector2f reach(Eigen::Vector2f current, float dt, Eigen::Vector2f goal) const;
  inline int random(int a, int b) const { return rand()%(b-a)+a; }
  Eigen::Vector2f projection(Eigen::Vector2f point, Eigen::Vector2f vec);
  Eigen::Vector2f angle(Eigen::Vector2f vec, float rad);
  
  std::pair<float, float> collideCircle (Eigen::Vector2f A
					 , Eigen::Vector2f B
					 , Eigen::Vector2f center
					 , float radius);

  inline void sceneNone() { m_current_scene = nullptr; }
  void sceneArena();
  void sceneLost();
  void sceneWon();
  void sceneMenu();
  void sceneTutorial();
  
  inline void quit() { m_window->close(); }
  
 protected:
  sf::RenderWindow* m_window;
  std::unique_ptr<ResourceCache> m_rsc;
  std::unique_ptr<PlayerControler> m_ctrl;
  
  Scene* m_current_scene;
  std::unique_ptr<ArenaScene> m_arena_scene;
  std::unique_ptr<LostScene> m_lost_scene;
  std::unique_ptr<WonScene> m_won_scene;
  std::unique_ptr<MenuScene> m_menu_scene;
  std::unique_ptr<TutorialScene> m_tutorial_scene;
  
  void loadRsc();
  
 private:
  Core( Core const& core ) = delete;
  Core& operator=( Core const& core ) = delete;
};

#endif
